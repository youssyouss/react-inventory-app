import * as React from 'react';
import shortid from 'shortid';

import {InventoryInterface, InventoryFormIterface} from './../interfaces';

import '../styles/styles.css';

const InventoryForm = (props: InventoryFormIterface) => {
    const inputRef = React.useRef<HTMLInputElement>(null)

    const [formState, setFormState] = React.useState('')

    function handleInputChange(event: React.ChangeEvent<HTMLInputElement>){
        setFormState(event.target.value);
    }

    function handleInputEnter(event: React.KeyboardEvent){
        if (event.key === 'Enter') {
            const newItem: InventoryInterface = {
                id: shortid.generate(),
                text: formState, 
                amount: 0, 
            }
            props.handleItemCreate(newItem);

            if (inputRef && inputRef.current){
                inputRef.current.value = ""
            }
        }
    }

    function handleSubmit(e: React.FormEvent<HTMLFormElement>){
        e.preventDefault();
        const newItem: InventoryInterface = {
            id: shortid.generate(),
            text: formState, 
            amount: 0, 
        }
        props.handleItemCreate(newItem);

        if (inputRef && inputRef.current){
            inputRef.current.value = ""
        }
    }

    return(
        <div className="todo-form">
            <form onSubmit={handleSubmit}>
            <input
                ref={inputRef}
                type="text"
                placeholder="Enter new item"
                onChange={event => handleInputChange(event)}
                onKeyPress={event => handleInputEnter(event)}
            />
            <button type='submit'>+</button>
            </form>
        </div>
    )
}

export default InventoryForm;