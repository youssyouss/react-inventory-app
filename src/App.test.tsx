import React from 'react';
import { render } from '@testing-library/react';
import InventoryApp from './index';

test('renders learn react link', () => {
  const { getByText } = render(<InventoryApp />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
