import * as React from 'react'; 
import { render } from 'react-dom'; 

import InventoryForm from './components/inventory-form'; 
import InventoryList from './components/inventory-list';
import { InventoryInterface } from './interfaces';


import './styles/styles.css'; 

const InventoryApp = () => {
  const [items, setItems] = React.useState<InventoryInterface[]>([]) 
    
  let localItems: InventoryInterface[] = [...items];
     if (localStorage.getItem('items') === null){
         localItems = [];
     } else {
         localItems = JSON.parse(localStorage.getItem('items') as string);
     }
    localStorage.setItem("items", JSON.stringify(localItems));


  function handleItemCreate(item: InventoryInterface) {
    const newItemState: InventoryInterface[] = [...items];

    newItemState.push(item);

    setItems(newItemState);
    localStorage.setItem("items", JSON.stringify(newItemState));
  }

  function handleItemUpdate(event: React.ChangeEvent<HTMLInputElement>, id: string){
    const newItemsState: InventoryInterface[] = [...items];
    
    newItemsState.find((item: InventoryInterface) =>  item.id === id)!.text = event.target.value;

    setItems(newItemsState);
  }

  function handleItemRemove(id: string){
    const newItemsState: InventoryInterface[] = items.filter((items:
      InventoryInterface) => items.id !== id);

      setItems(newItemsState);
  }

  function handleIncrement(id: string){
    const newItemsState: InventoryInterface[] = [...items]

    newItemsState.find((item: InventoryInterface) => item.id === id)!.amount 
    = newItemsState.find((item: InventoryInterface) => item.id === id)!.amount + 1;

    setItems(newItemsState);
  }


  function handleDecrement(id: string){
    const newItemsState: InventoryInterface[] = [...items]

    newItemsState.find((item: InventoryInterface) => item.id === id)!.amount 
    = newItemsState.find((item: InventoryInterface) => item.id === id)!.amount - 1;

    setItems(newItemsState);
  }

  function handleItemBlur(event: React.ChangeEvent<HTMLInputElement>){
    if(event.target.value.length === 0){
      event.target.classList.add('todo-input-error');
    } else {
      event.target.classList.remove('todo-input-error');
    }
  }

  return (
    <div className="todo-list-app">
      <h1> Youssra's Inventory list </h1>
      <InventoryForm
      inventory={items}
      handleItemCreate={handleItemCreate} 
      />
      <InventoryList
        items={items}
        handleItemUpdate={handleItemUpdate}
        increment={handleIncrement}
        decrement={handleDecrement}
        handleItemRemove={handleItemRemove}
        handleItemBlur={handleItemBlur}
       />
  
    </div>
  )
}


const rootElement = document.getElementById('root');
render(<InventoryApp />, rootElement);

export default InventoryApp;