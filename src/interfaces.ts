// Inventory interface

export interface InventoryInterface {
    id: string; 
    text: string; 
    amount: number;
}

export interface InventoryFormIterface {
    inventory: InventoryInterface[];
    handleItemCreate: (item: InventoryInterface) => void;
}

export interface ItemListInterface {
    handleItemUpdate: (event: React.ChangeEvent<HTMLInputElement>, id: string) => void; 
    handleItemRemove: (id: string) => void;
    increment: (id: string) => void;
    decrement: (id: string) => void;
    handleItemBlur: (event: React.ChangeEvent<HTMLInputElement>) => void; 
    items: InventoryInterface[];
}

export interface InventoryItemInterface {
    handleItemUpdate: (event: React.ChangeEvent<HTMLInputElement>, id: string) => void;
    handleItemRemove: (id: string) => void;
    increment: (id: string) => void;
    decrement: (id: string) => void;    
    handleItemBlur: (event: React.ChangeEvent<HTMLInputElement>) => void; 
    item: InventoryInterface;
}