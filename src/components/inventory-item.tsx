import * as React from 'react';


import { InventoryItemInterface } from './../interfaces';

const InventoryItem = (props: InventoryItemInterface) => {
    return (
        <div className="todo-item">
             <div className="counter grid">
                <button id="increment" className="grid-item" onClick={() => props.increment(props.item.id)}>+</button>
                <p className="grid-item">{props.item.amount}</p>
                <button id="decrement" className="grid-item" onClick={() => props.decrement(props.item.id)}>-</button>
            </div>
            <div className="todo-item-input-wrapper">
                <input
                    value={props.item.text}
                    onBlur={props.handleItemBlur}
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => 
                    props.handleItemUpdate(event, props.item.id)}
                />
            </div>
            <button className="item-remove" onClick={() => props.handleItemRemove(props.item.id)}>
                X
            </button>
        </div>
    )
}

export default InventoryItem;