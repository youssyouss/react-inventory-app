import * as React from 'react'; 

import InventorItem from './inventory-item'; 

import { ItemListInterface } from './../interfaces';

const InventoryList = (props: ItemListInterface) => {
    return (
        <div className="todo-list">
            <ul>
                {props.items.map((item) => (
                    <li key={item.id}>
                        <InventorItem 
                        item = {item}
                        handleItemUpdate={props.handleItemUpdate}
                        handleItemRemove={props.handleItemRemove}
                        increment={props.increment}
                        decrement={props.decrement}
                        handleItemBlur={props.handleItemBlur}
                        />
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default InventoryList;